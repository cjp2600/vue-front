/**
 * Application configuration
 *
 * @type {{ app_title: string, api_url: string, api_header_prefix: string, client_id: number, client_secret: string, is_use_refresh_token: boolean, default_lang: string}}
 */
const configs = {

    /**
     * APPLICATION TITLE
     */
    app_title: "Waity",

    /**
     * API URL
     */
    api_url: 'https://api.waity.ru',

    /**
     * API PREFIX (LARAVEL PASSPORT)
     */
    api_header_prefix: "Bearer",

    /**
     * AUTH PASSPORT CLIENT ID
     */
    client_id: 2,

    /**
     * AUTH PASSPORT CLIENT SECRET
     */
    //client_secret: 'bODQsqKz184wB2yOxlRVyWOzd09QLXaNFk6qqUEt',
    client_secret: 'bg1VbuKB2HgameGMHWXVOh8HJmgE8Gk59oNwnCnI',

    /**
     * IS USE REFRESH TOKEN
     */
    is_use_refresh_token: true,

    /**
     * DEFAULT LANG
     */
    default_lang: "ru"

};

export default configs;