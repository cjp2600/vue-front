import Vue from 'vue'
import App from './App.vue'
import Router from './routes.js'
import VueResource from 'vue-resource'
import Lang from 'vuejs-localization'
import VueConfig from 'vue-config'
import Config from './config/app.js'
import Auth from './packages/auth/auth.js'
import User from './packages/user/user.js'
import Service from './packages/services/service.js'

import VueResourceNProgress from 'vue-resource-nprogress'
import {store} from './vuex/store.js'

Vue.use(VueResource);
Vue.use(VueResourceNProgress);
Vue.use(VueConfig, Config);

Vue.use(Auth);
Vue.use(User);
Vue.use(Service);

/**
 * Set localization
 */
Lang.requireAll(require.context('./lang', true, /\.js$/));
Vue.use(Lang);

/**
 * HTTP DEFAULT CONFIGURATION
 * @type {string}
 */
Vue.http.options.root = Config.api_url;
Vue.http.headers.common['Authorization'] = Config.api_header_prefix + ' ' + Vue.auth.getToken();

/**
 * ROUTER CONFIGURATION
 */
Router.beforeEach(
    (to, from, next) => {

        if (to.matched.some(record => record.meta.forAll)) {
            next()
        }

        else if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuth()) {
                next({
                    path: '/feed'
                })
            } else next()
        }

        else if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuth()) {
                next({
                    path: '/login'
                })
            } else next()
        }

        else next()
    }
);

/**
 * SET TITLE
 */
Router.beforeEach((to, from, next) => {
    document.title = (to.meta.title.length) ? Config.app_title + " | " + to.meta.title : Config.app_title;
    next();
});

/**
 * INITIALIZATION
 */
new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router: Router
});

