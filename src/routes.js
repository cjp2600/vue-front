
import Vue from 'vue'
import VueRouter from 'vue-router'

/**
 * PAGES COMPONENT
 */
import LoginComponent from './views/auth/login.vue'
import RegisterComponent from './views/auth/register.vue'
import FeedComponent from './views/profile/feed.vue'
import ProfileEditComponent from './views/profile/settings.vue'
import AccountEditComponent from './views/profile/settings.account.edit.vue'
import AccountPayComponent from './views/profile/settings.account.pay.vue'
import HomePageComponent from './views/home.vue'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomePageComponent,
            meta: {
                forAll: true,
                title: ""
            }
        },
        {
            path: '/login',
            component: LoginComponent,
            meta: {
                forVisitors: true,
                title: "Авторизация"
            }
        },
        {
            path: '/register',
            component: RegisterComponent,
            meta: {
                forVisitors: true,
                title: "Регистрация"
            }
        },
        {
            path: '/feed',
            component: FeedComponent,
            meta: {
                forAuth: true,
                title: "Профиль"
            }
        },
        {
            path: '/settings',
            component: ProfileEditComponent,
            meta: {
                forAuth: true,
                title: "Настройки"
            },
            children: [
                {
                    path: 'user',
                    component: AccountEditComponent,
                    meta: {
                        forAuth: true,
                        title: "Редактирование аккаунта"
                    },
                },
                {
                    path: 'pay',
                    component: AccountPayComponent,
                    meta: {
                        forAuth: true,
                        title: "Оплата аккаунта"
                    },
                }
            ]
        }
    ]
});

export default router