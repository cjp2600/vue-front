/**
 * Модуль для работы с данными пользователя
 *
 * @param Vue
 * @returns {*}
 */
export default function (Vue) {
    Vue.user = {

        /**
         * Получение полной информации о пользователе
         * без сохранения в storage
         *
         * @param context (Component context)
         * @returns {{}}
         */
        getInfo(context) {
            if (context.$auth.isAuth()) {
                Vue.http.get('api/user/preview', {showProgressBar: true})
                    .then(response => {
                        context.user = response.body.data;
                    });
            }
        },

        /**
         * Сохранение профиля пользователя и обновление языка
         *
         * @param context
         * @param data
         * @param callback
         */
        storeProfile(context, data, callback) {
            context.$http.post('api/user/profile', data)
                .then(response => {

                    if (typeof callback !== "undefined") {
                        callback(response.body);
                    }

                    let setUserLang = (data) => {
                        context.$service.setLang(context)
                    };
                    context.$store.dispatch('getUser',setUserLang);

                }, response => {
                    context.validationMessage = response.body.message;
                });
        }


    };

    Object.defineProperties(Vue.prototype, {
        $user: {
            get: () => {
                return Vue.user;
            }
        }
    })
}