/**
 * Модуль для работы с мультиязычностью
 *
 * @param Vue
 * @returns {*}
 */
export default function (Vue) {
  Vue.service = {

    /**
     * Установка языка исходя из профиля пользователя
     *
     * @returns {*}
     */
    setLang(context) {
      return context.$lang.setLang(this.getLangCode(context))
    },

    /**
     * Получение языка пользователя
     *
     * @param context
     * @returns {string}
     */
    getLangCode(context) {
      return (context.$auth.isAuth())
        ? context.$store.getters.user.lang
        : context.$config.default_lang
    },


  };
  Object.defineProperties(Vue.prototype, {
    $service: {
      get: () => {
        return Vue.service
      },
    },
  })
}