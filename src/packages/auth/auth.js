import {store} from '../../vuex/store.js'

/**
 * Auth packages
 *
 * @param Vue
 * @returns {*}
 */
export default function (Vue) {
    Vue.auth = {

        /**
         * Authorization user
         *
         * @param context
         */
        login(context)
        {
            let data = {
                client_id: context.$config.client_id,
                client_secret: context.$config.client_secret,
                grant_type: 'password',
                username: context.email,
                password: context.password
            };

            context.$http.post('oauth/token', data)
                .then(response => {
                    console.log('user is login');

                    context.$store.dispatch('setToken', {
                        token: response.body.access_token,
                        expiration: response.body.expires_in + Date.now(),
                        refresh_token: response.body.refresh_token
                    });
                    context.$router.push("/feed");

                    let callbackUser = (data) => {
                        context.$service.setLang(context);
                    }

                    context.$store.dispatch('getUser',callbackUser.bind(context));
                }, response => {
                    context.validationMessage = response.body.message;
                });
        },

        /**
         * Registration user
         *
         * @param context
         * @param isAuth
         */
        register(context, isAuth = true)
        {
            let data = {
                first_name: context.first_name,
                last_name: context.last_name,
                email: context.email,
                password: context.password,
                password_confirmation: context.confirm_password,
            };
            context.$http.post('api/register', data)
                .then(response => {
                    if (response) {
                        if (isAuth) {
                            context.$auth.login(context);
                        } else {
                            context.$router.push("/login");
                        }
                    }
                }, response => {
                    // if validation code
                    if (response.status === 422) {
                        let result = [];
                        for ( const key in response.body ) {
                            result.push(response.body[key]);
                        }
                        context.validationMessage = result.join('<br>');
                    } else {
                        context.validationMessage = response.body.meta.message
                    }
                });
        },

        /**
         * Get access token from vuex and check expiration
         *
         * @returns {*}
         */
        getToken() {
            let token = store.getters.token;
            let expiration = store.getters.expiration;

            if (!token || !expiration) {
                return null;
            }

            if (Date.now() > parseInt(expiration)) {
                store.dispatch('removeToken');
                return null;
            }
            return token;
        },

        /**
         * Check is Auth user
         *
         * @returns {boolean}
         */
        isAuth() {
            return !!this.getToken();
        },

        /**
         * Refresh Token with timer
         *
         * @param context
         * @param time
         */
        refreshToken(context, time = null)
        {
            if (!this.isAuth() || !context.$config.is_use_refresh_token) {
                return null;
            }

            this.queryToRefreshToken(context);
            if (time) {
                setInterval(function () {
                    this.queryToRefreshToken(context);
                }.bind(this, context), time);
            }
        },

        /**
         * Query to refresh token
         *
         * @param context
         * @returns {null}
         */
        queryToRefreshToken(context)
        {
            let data = {
                client_id: context.$config.client_id,
                client_secret: context.$config.client_secret,
                grant_type: 'refresh_token',
                refresh_token: context.$store.getters.refreshToken,
                scope: ''
            };

            context.$http.post('oauth/token', data, { showProgressBar: false })
                .then(response => {
                    context.$store.dispatch('setToken', {
                        token: response.body.access_token,
                        expiration: response.body.expires_in + Date.now(),
                        refresh_token: response.body.refresh_token
                    });
                    console.log('token is updated');
            });
        }
    };

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth;
            }
        }
    });
}