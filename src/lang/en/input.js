module.exports = {

    password: 'Password',
    first_name: 'First Name',
    last_name: 'Last Name',
    confirm: 'Confirm',
    date_of_b: 'Date of Birth',
    lang: 'Language',
    select_hint: 'Choose one of the options'
}