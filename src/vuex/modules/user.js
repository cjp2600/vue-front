/**
 * USER MODULES
 */
import Vue from 'vue'
import Persister from '../persister'

const persister = new Persister('user', {provider: localStorage});
export const userModule = {
    state: {
        user: {
            id: persister.get('id') || null,
            fullName: persister.get('name') || null,
            avatar: persister.get('avatar') || null,
            age: persister.get('avatar') || null,

            firstName: null,
            lastName: null,
            email: null,
            dateBirth: null,
            lang: persister.get('lang') || null,
        }
    },
    actions: {

        getUser({commit}, callback) {
            Vue.http.get('api/user/preview', { showProgressBar: false } )
                .then(response => {
                    commit('SET_USER', response.body.data);
                    if (typeof callback !== "undefined") {
                        callback(response.body.data);
                    }
                });
        },

        removeUser({commit})
        {
            commit('REMOVE_USER');
        }
    },
    mutations: {
        SET_USER(state, user) {

            console.log(user);

            state.user.id = user.id;
            state.user.fullName = user.fullName;
            state.user.avatar = user.avatar;
            state.user.age = user.age;
            state.user.firstName = user.firstName;
            state.user.lastName = user.lastName;
            state.user.email = user.email;
            state.user.dateBirth = user.dateBirth;
            state.user.lang = user.lang;

            persister.set('id', user.id);
            persister.set('name', user.fullName);
            persister.set('avatar', user.avatar);
            persister.set('age', user.age);
            persister.set('lang', user.lang);
        },
        REMOVE_USER(state) {
            state.user.id = null;
            state.user.fullName = null;
            state.user.avatar = null;
            state.user.age = null;
            state.user.firstName = null;
            state.user.lastName = null;
            state.user.email = null;
            state.user.dateBirth = null;
            state.user.lang = null;

            persister.remove('id');
            persister.remove('name');
            persister.remove('avatar');
            persister.remove('age');
            persister.remove('lang');
        }
    },
    getters: {
        user(state) {
            return state.user;
        }
    }
};