/**
 * AUTH MODULES
 */
import Vue from 'vue'
import Persister from '../persister'

const persister = new Persister('auth', {provider: localStorage});
export const authModule = {
    state: {
        token: persister.get('token') || null,
        expiration: persister.get('expiration') || null,
        refresh_token: persister.get('refresh_token') || null,
    },
    actions: {
        setToken({commit}, data) {
            commit('SET_AUTH', data);
        },
        removeToken({commit}){
            commit('REMOVE_TOKEN');
        }
    },
    mutations: {
        SET_AUTH(state, data) {
            state.token = data.token;
            state.expiration = data.expiration;
            state.refresh_token = data.refresh_token;

            Vue.http.headers.common['Authorization'] = 'Bearer ' + state.token;

            persister.set('token', state.token);
            persister.set('expiration', state.expiration);
            persister.set('refresh_token', state.refresh_token);
        },
        REMOVE_TOKEN(state) {

            state.token = null;
            state.expiration = null;
            state.refresh_token = null;

            persister.remove('token');
            persister.remove('expiration');
            persister.remove('refresh_token');
        }
    },
    getters: {
        token(state) {
            return state.token;
        },
        expiration(state) {
            return state.expiration;
        },
        refreshToken(state) {
            return state.refresh_token;
        }
    }
};