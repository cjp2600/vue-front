import Vue from 'vue'
import Vuex from 'vuex'
import { authModule } from './modules/auth.js'
import { userModule } from './modules/user.js'

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        auth: authModule,
        user: userModule
    }
});