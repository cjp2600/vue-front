import moment from 'moment';

/**
 * PERSISTER CLASS
 */
export default
class Persister {

    /**
     * Default constructor
     *
     * @param namePrefix
     * @param provider
     * @param serialize
     * @param deserialize
     * @param ttl
     */
    constructor(namePrefix = '', {provider = localStorage, serialize = JSON.stringify, deserialize = JSON.parse, ttl = -1}) {
        this.namePrefix = namePrefix;
        this.provider = provider;
        this.serialize = serialize;
        this.deserialize = deserialize;
        this.ttl = ttl;
    }

    /**
     * Getter store value
     * @param key
     * @returns {*}
     */
    get(key) {
        const fullKey = `${this.namePrefix}::${key}`;
        const rawData = this.provider.getItem(fullKey);
        const {ttl, createdAt, data} = Object.assign({
            ttl: -1,
            created_at: moment,
            data: "null"
        }, this.deserialize(rawData));

        if (ttl === -1 || moment() < moment(createdAt).add(ttl, 'm')) {
            return this.deserialize(data);
        }

        return null
    }

    /**
     * Setter store value
     * @param key
     * @param value
     */
    set(key, value) {
        const fullKey = `${this.namePrefix}::${key}`;
        const createdAt = moment();
        const ttl = this.ttl;
        const data = this.serialize(value);

        const rawData = this.serialize({data, ttl, createdAt});
        this.provider.setItem(fullKey, rawData)
    }

    /**
     * Remove store value
     * @param key
     */
    remove(key) {
        const fullKey = `${this.namePrefix}::${key}`;

        this.provider.removeItem(fullKey);
    }
}